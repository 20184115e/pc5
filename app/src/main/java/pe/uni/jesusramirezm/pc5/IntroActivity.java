package pe.uni.jesusramirezm.pc5;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class IntroActivity extends AppCompatActivity {

    TextView textViewIntro, textViewWelcome;
    RelativeLayout imageViewBackground;
    Animation animationTextViewIntro;

    String nameIntent;
    int backgroundIntent;

    // Para guardar después del primer ingreso
    String name;
    int background;
    SharedPreferences sharedPreferences;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        // Para concatenar cadenas en el XML
        Resources res = getResources();

        textViewIntro = findViewById(R.id.text_view_intro);
        textViewWelcome = findViewById(R.id.text_view_welcome);
        imageViewBackground = findViewById(R.id.screen);

        // Enlazando animaciones
        animationTextViewIntro = AnimationUtils.loadAnimation(this, R.anim.text_intro_animation);
        textViewIntro.setAnimation(animationTextViewIntro);


        /* Debemos verificar si es la primera vez que el usuario ingresa, para saber si
        debe ir a la pantalla de configuración o a la pantalla principal */

        // Recuperamos la data
        retrieveData();

        // Primera vez
        // Ya que no siempre venimos de un Intent, también evaluamos que no tengamos nombre guardado
        if (getIntent().getExtras() == null && nameIntent == null) {

            textViewWelcome.setText(String.format(res.getString(R.string.text_view_welcome), ""));
            imageViewBackground.setBackgroundResource(R.color.black);
            // Timer
            new CountDownTimer(5000, 1000) {

                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {

                    // Intent para pasar a otra actividad. En este caso, Configuration.

                    Intent intent = new Intent(IntroActivity.this, ConfigurationActivity.class);
                    startActivity(intent);
                    finish();
                }

            }.start();
        }

        // Si ya hemos recibido datos
        else {

            // Obtenemos la instancia que se realizó. Con ello también los datos
            Intent intent = getIntent();

            if (getIntent().getExtras() != null) {
                nameIntent = intent.getStringExtra("TEXT");
                backgroundIntent = intent.getIntExtra("NUMBER", 0);
            }

            textViewWelcome.setText(String.format(res.getString(R.string.text_view_welcome), nameIntent));


            switch (backgroundIntent) {

                case 1: imageViewBackground.setBackgroundResource(R.drawable.solar);
                        textViewWelcome.setShadowLayer(30,30,30,R.color.black); break;

                case 2: imageViewBackground.setBackgroundResource(R.drawable.space);
                        textViewWelcome.setShadowLayer(30,30,30,R.color.black); break;

                case 3: imageViewBackground.setBackgroundResource(R.drawable.heart);
                        textViewWelcome.setShadowLayer(30,30,30,R.color.black); break;

                case 4: imageViewBackground.setBackgroundResource(R.drawable.magia);
                        textViewWelcome.setShadowLayer(30,30,30,R.color.black); break;

                case 5: imageViewBackground.setBackgroundResource(R.drawable.night);
                        textViewWelcome.setShadowLayer(30,30,30,R.color.black); break;

            }

            // Timer
            new CountDownTimer(5000, 1000) {

                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {

                    // Intent para pasar a otra actividad. En este caso, Main.
                    Intent intent = new Intent(IntroActivity.this, SongListActivity.class);
                    startActivity(intent);
                    finish();
                }

            }.start();
        }
    }

    // FUNCIÓN POR DONDE LA APLICACIÓN SIEMPRE PASA
    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }

    // Ahora veremos como guardar estos datos, incluso si cerramos la aplicación.
    private void saveData() {

        // Instancia
        sharedPreferences = getSharedPreferences("saveData", Context.MODE_PRIVATE);

        // Datos a guardar
        name = nameIntent;
        background = backgroundIntent;
        SharedPreferences.Editor editor = sharedPreferences.edit();

        // Llaves asociadas con las que recuperaremos los datos
        editor.putString("key name", name);
        editor.putInt("key image", background);
        editor.apply();
    }

    // Para recuperar los datos guardados
    private void retrieveData() {

        // Instancia
        sharedPreferences = getSharedPreferences("saveData", Context.MODE_PRIVATE);

        // Recuperando datos con las llaves
        name = sharedPreferences.getString("key name", null);
        background = sharedPreferences.getInt("key image", 0);

        /* NOTA: PARA COMPROBAR EL FUNCIONAMIENTO DE LOS DATOS GUARDADOS,
        COMENTA LAS LINEAS SIGUIENTES Y EJECUTA LA APLICACIÓN HASTA
        LLEGAR A LA PANTALLA DE CONFIGURACIÓN, LUEGO CIÉRRALA,
        DESCOMENTA LA LÍNEA Y EJECUTA.
         */
       nameIntent = name;
       backgroundIntent = background;


    }

}