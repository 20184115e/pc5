package pe.uni.jesusramirezm.pc5;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Size;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;

public class ListViewAdapter extends BaseAdapter {

    Context context;
    ArrayList<String> title;
    ArrayList<String> artist;
    ArrayList<Uri> uri;
    ArrayList<Integer> duration;


    // Constructor
    public ListViewAdapter(Context context, ArrayList<String> title, ArrayList<String> artist, ArrayList<Uri> uri, ArrayList<Integer> duration) {

        this.context = context;
        this.title = title;
        this.artist = artist;
        this.uri = uri;
        this.duration = duration;

    }

    @Override
    public int getCount() {
        // Tamaño del arreglo de textos
        return title.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Para obtener las vistas
        @SuppressLint("ViewHolder") View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_view_adapter, parent, false);

        TextView textViewTitle = view.findViewById(R.id.song_title);
        TextView textViewArtist = view.findViewById(R.id.song_artist);
        ImageView imageViewAlbumCover = view.findViewById(R.id.song_album_cover);
        TextView textViewDuration = view.findViewById(R.id.song_duration);

        textViewTitle.setText(title.get(position));
        textViewArtist.setText(artist.get(position));

        Resources res = context.getResources();
        String minutos = String.valueOf((duration.get(position)/1000) / 60);
        String segundos = String.valueOf((duration.get(position)/1000) % 60);

        if (segundos.length() == 1) {
            segundos = "0" + segundos;
        }

        textViewDuration.setText(String.format(res.getString(R.string.duration), minutos, segundos));

        // API: 29
            try {
                Bitmap thumbnail = context.getApplicationContext().getContentResolver().loadThumbnail(uri.get(position), new Size(500, 500), null);
                imageViewAlbumCover.setImageBitmap(thumbnail);

            } catch (IOException e) {
                e.printStackTrace();
            }

        return view;
    }
}
