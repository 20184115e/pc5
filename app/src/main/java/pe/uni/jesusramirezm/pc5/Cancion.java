package pe.uni.jesusramirezm.pc5;

import android.net.Uri;

public class Cancion {

    public Uri uri;
    public String titulo;
    public String artista;
    public int duracion;

    public Cancion(Uri uri, String titulo, String artista, int duracion) {
        this.uri = uri;
        this.titulo = titulo;
        this.artista = artista;
        this.duracion = duracion;

    }
}
