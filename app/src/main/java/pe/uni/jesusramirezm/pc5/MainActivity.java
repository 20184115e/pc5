package pe.uni.jesusramirezm.pc5;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Size;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

// Imagen circular
import java.io.IOException;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity {

    CircleImageView imageAlbum;
    TextView titleSong, artistSong, durationSong, timePassed;
    Button playButton, nextButton, backButton;
    SeekBar seekBar;
    // Static para poder acceder a la variable desde otras clases
    static MediaPlayer mediaPlayer;
    Animation animationImage;
    ArrayList<Cancion> lista = new ArrayList<>();
    int posicionActual;
    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageAlbum = findViewById(R.id.image_album);
        playButton = findViewById(R.id.button_play);
        nextButton = findViewById(R.id.button_next);
        backButton = findViewById(R.id.button_back);
        seekBar = findViewById(R.id.seek_bar);

        titleSong = findViewById(R.id.main_song_title);
        artistSong = findViewById(R.id.main_song_artist);
        durationSong = findViewById(R.id.final_time);
        timePassed = findViewById(R.id.initial_time);

        lista = SongListActivity.listaCanciones;

        // Enlazando animaciones
        animationImage = AnimationUtils.loadAnimation(this, R.anim.image_album_animation);
        imageAlbum.setAnimation(animationImage);

        // Obtenemos la instancia que se realizó. Con ello también los datos
        Intent intent = getIntent();
        posicionActual = intent.getIntExtra("ACTUAL POSITION", 0);

        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
        }
        establecerCancion();


        playButton.setOnClickListener(v -> {

            // Si hemos presionado el botón y la música está sonando, pausamos
            if (mediaPlayer.isPlaying()) {

                mediaPlayer.pause();
                playButton.setBackgroundResource(R.drawable.play);
                Toast.makeText(getApplicationContext(), R.string.msg_toast_pause, Toast.LENGTH_SHORT).show();
            }

            // Si hemos presionado el botón y la música no está sonando, continuamos
            else if (!mediaPlayer.isPlaying()) {
                mediaPlayer.start();
                playButton.setBackgroundResource(R.drawable.pause);
                Toast.makeText(getApplicationContext(), R.string.msg_toast_play, Toast.LENGTH_SHORT).show();
            }

        });

        backButton.setOnClickListener(v -> {

            mediaPlayer.stop();
            mediaPlayer.release();
            playButton.setBackgroundResource(R.drawable.pause);

            if (posicionActual == 0) { posicionActual = lista.size()-1; }
                else posicionActual = posicionActual - 1;
            establecerCancion();
        });

        nextButton.setOnClickListener(v -> {

            mediaPlayer.stop();
            mediaPlayer.release();
            playButton.setBackgroundResource(R.drawable.pause);

            if (posicionActual  == lista.size() - 1) { posicionActual = 0; }
                else posicionActual = posicionActual + 1;
            establecerCancion();
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                // Acción realizada por el usuario
                if (mediaPlayer != null && fromUser) {
                    mediaPlayer.seekTo(progress*1000);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

        });
    }

    protected void establecerCancion() {

        // Carátula de la canción
        // API: 29
        try {
            Bitmap thumbnail = getApplicationContext().getContentResolver().loadThumbnail(lista.get(posicionActual).uri, new Size(500, 500), null);
            imageAlbum.setImageBitmap(thumbnail);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Título y artista
        titleSong.setText(lista.get(posicionActual).titulo);
        artistSong.setText(lista.get(posicionActual).artista);

        Resources res = getResources();

        // Duración
        String minutos = String.valueOf((lista.get(posicionActual).duracion/1000) / 60);
        String segundos = String.valueOf((lista.get(posicionActual).duracion/1000) % 60);

        if (segundos.length() == 1) {
            segundos = "0" + segundos;
        }

        durationSong.setText(String.format(res.getString(R.string.duration_main), minutos, segundos));

        // Seekbar
        seekBar.setMax(lista.get(posicionActual).duracion / 1000);
        seekBar.setMin(0);

        // MediaPlayer
        mediaPlayer = MediaPlayer.create(this, lista.get(posicionActual).uri);
        mediaPlayer.start();

        // Cuando termine la canción
        mediaPlayer.setOnCompletionListener(mp -> nextButton.performClick());

        // Hilo principal
        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mediaPlayer != null) {

                    int tiempoActual = mediaPlayer.getCurrentPosition()/1000;
                    seekBar.setProgress(tiempoActual);
                    timePassed.setText(tiempoSeekBar(tiempoActual));
                }
                handler.postDelayed(this, 1000);
            }
        });


    }

    protected String tiempoSeekBar(int tiempoActual) {

        // Duración
        String minutos = String.valueOf(tiempoActual / 60);
        String segundos = String.valueOf(tiempoActual % 60);

        if (segundos.length() == 1) {
            segundos = "0" + segundos;
        }

        return minutos + ":" + segundos;


    }
}