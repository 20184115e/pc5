package pe.uni.jesusramirezm.pc5;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.ContentUris;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;


public class SongListActivity extends AppCompatActivity {

    ListView listView;
    TextView textView;

    // Static para poder acceder a la variable desde otras clases
    static ArrayList<Cancion> listaCanciones = new ArrayList<>();

    ArrayList<String> title = new ArrayList<>();
    ArrayList<String> artist = new ArrayList<>();
    ArrayList<Uri> uri = new ArrayList<>();
    ArrayList<Integer> duration = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_song_list);

        listView = findViewById(R.id.list_view);
        textView = findViewById(R.id.text_view_main);

        if (!evaluarPermiso()) {
            solicitarPermiso();
            return;
        }

        Uri collection;

        // API 29
        collection = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;


        // Ruta, Nombre y duración
        String[] projection = {
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.ARTIST
        };

        // Canciones que duren más de un minuto
        String selection = MediaStore.Audio.Media.DURATION +
                " >= ?";
        String[] selectionArgs = new String[] {
                String.valueOf(TimeUnit.MILLISECONDS.convert(1, TimeUnit.MINUTES))
        };

        // Ordenamiento
        String sortOrder = MediaStore.Audio.Media.DISPLAY_NAME + " ASC";

        // Realizamos la consulta
        try (Cursor cursor = getApplicationContext().getContentResolver().query(
                collection,
                projection,
                selection,
                selectionArgs,
                sortOrder
        )) {
            // Cache column indices.
            int idColumn = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media._ID);
            int nameColumn =
                    cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.TITLE);
            int durationColumn =
                    cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DURATION);
            int artistColumn =
                    cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.ARTIST);


            while (cursor.moveToNext()) {
                // Get values of columns for a given video.
                long id = cursor.getLong(idColumn);
                String name = cursor.getString(nameColumn);
                int duration = cursor.getInt(durationColumn);
                String artist = cursor.getString(artistColumn);
                Uri contentUri = ContentUris.withAppendedId(
                        MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, id);

                // Stores column values and the contentUri in a local object
                // that represents the media file.
                listaCanciones.add(new Cancion(contentUri, name, artist, duration));
            }
        }

        for (int i = 0; i < listaCanciones.size(); i++) {
            Cancion cancion = listaCanciones.get(i);
            title.add(cancion.titulo);
            artist.add(cancion.artista);
            uri.add(cancion.uri);
            duration.add(cancion.duracion);
        }

        ListViewAdapter listViewAdapter = new ListViewAdapter(this, title, artist, uri, duration);
        listView.setAdapter(listViewAdapter);

        // Al hacer click en uno de los ítems
        listView.setOnItemClickListener((parent, view, position, id) -> {

            if (MainActivity.mediaPlayer != null) {
                MainActivity.mediaPlayer.stop();
                MainActivity.mediaPlayer.release();
            }
            MainActivity.mediaPlayer = null;

            // Segunda actividad
            Intent intent = new Intent( SongListActivity.this, MainActivity.class);

            // Le damos el dato a la segunda actividad
            intent.putExtra("ACTUAL POSITION", position);
            startActivity(intent);

        });

    }

    protected boolean evaluarPermiso() {

        int result = ContextCompat.checkSelfPermission(SongListActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    protected void solicitarPermiso() {

        ActivityCompat.requestPermissions(SongListActivity.this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                123);
    }
}