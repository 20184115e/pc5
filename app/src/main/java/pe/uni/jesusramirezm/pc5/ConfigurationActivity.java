package pe.uni.jesusramirezm.pc5;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;


public class ConfigurationActivity extends AppCompatActivity {

    Button button;
    EditText editTextName;
    Spinner spinnerLogo;
    ArrayAdapter<CharSequence> adapter;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);

        editTextName = findViewById(R.id.edit_text_name);
        button = findViewById(R.id.button);
        spinnerLogo = findViewById(R.id.spinner);

        adapter = ArrayAdapter.createFromResource(this, R.array.logos, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLogo.setAdapter(adapter);


        button.setOnClickListener(v -> {

            // Convertimos las entradas a cadenas
            String sText = editTextName.getText().toString();
            int spinnerChoose = spinnerLogo.getSelectedItemPosition() + 1;

            // Segunda actividad --> Activity Intro
            Intent intent = new Intent( ConfigurationActivity.this, IntroActivity.class);

            // Le damos el dato a la actividad de configuración
            intent.putExtra("TEXT", sText);
            intent.putExtra("NUMBER", spinnerChoose);

            startActivity(intent);
            finish();
        });

    }


}